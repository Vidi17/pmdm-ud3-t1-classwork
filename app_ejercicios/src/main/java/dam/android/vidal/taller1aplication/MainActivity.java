package dam.android.vidal.taller1aplication;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private int count;

    private TextView tvDisplay;
    private Button buttonIncrease, buttonIncrease2, buttonDecrease, buttonDecrease2, buttonReset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState != null){
            count = savedInstanceState.getInt("count");
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override protected void onSaveInstanceState(Bundle guardarEstado) {
        guardarEstado.putInt("count", count);
        super.onSaveInstanceState(guardarEstado);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonIncrease: count++; break;
            case R.id.buttonIncrease2: count += 2; break;
            case R.id.buttonDecrease: count--; break;
            case R.id.buttonDecrease2: count -= 2; break;
            case R.id.buttonReset: count = 0; break;
        }

        tvDisplay.setText(getString(R.string.number_of_elements) + ": " + count);
    }

    private void setUI(){
        tvDisplay = findViewById(R.id.tvDisplay);
        tvDisplay.setText(getString(R.string.number_of_elements) + ": " + count);
        buttonIncrease = findViewById(R.id.buttonIncrease);
        buttonIncrease2 = findViewById(R.id.buttonIncrease2);
        buttonDecrease = findViewById(R.id.buttonDecrease);
        buttonDecrease2 = findViewById(R.id.buttonDecrease2);
        buttonReset = findViewById(R.id.buttonReset);

        buttonIncrease.setOnClickListener(this);
        buttonIncrease2.setOnClickListener(this);
        buttonDecrease.setOnClickListener(this);
        buttonDecrease2.setOnClickListener(this);
        buttonReset.setOnClickListener(this);
    }
}