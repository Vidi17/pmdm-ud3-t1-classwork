package dam.android.vidal.taller1aplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;

public class SplashActivity extends AppCompatActivity {

    private MediaPlayer sound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getSupportActionBar().hide();

        Thread timer = new Thread(() -> {
            try {
                Thread.sleep(1000);
                sound = MediaPlayer.create(getApplicationContext(), R.raw.ankaramessi);
                sound.start();
                Thread.sleep(5000);
                sound.stop();
            }catch (InterruptedException e){
                System.err.println(e.getMessage());
            }finally {
                startActivity(new Intent("dam.android.vidal.taller1aplication.STARTINGPOINT"));
            }
        });
        timer.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}